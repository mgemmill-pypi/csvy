from pathlib import Path
from itertools import product
import pytest
import csvy
from csvy import _csvy
from string import ascii_letters
from random import randint, choice

@pytest.fixture
def tempfile(tmpdir):
    file_ = Path(tmpdir, 'test.csv')
    yield file_
    file_.unlink()


@pytest.fixture
def csv_file(tempfile):
    with tempfile.open(mode='w') as fh_:
        fh_.write('"A", "B", "C"\n')
        fh_.write('"one", "two", "three"\n')
    yield tempfile


@pytest.fixture
def csv_file_2(tempfile):
    with tempfile.open(mode='w') as fh_:
        fh_.write('"A", "", "C", "3rd", "A"\n')
        fh_.write('"one", "two", "three", "four", "four.one"\n')
        fh_.write('"five", "six", "seven", "eight", "eight.one"\n')
    yield tempfile


def test_csvy_write(tempfile):
    with csvy.writer(tempfile) as csv:
        csv.writerow(('A', 'B', 'C'))
        csv.writerow((1, 2, 3))

    content = tempfile.read_text()
    assert content == "A,B,C\n1,2,3\n"


def test_csvy_read(csv_file):

    with csvy.reader(csv_file) as csv:
        assert csv.has_header is True
        assert csv.dialect.delimiter == ','

        gen = csv.iter(skip_header=False, namedtuple=False)

        i, r = next(gen)
        assert i == 0
        assert r == ("A", "B", "C")

        i, r = next(gen)
        assert i == 1
        assert r == ("one", "two", "three")


def test_csvy_read_with_blank_header_columns(csv_file_2):

    with csvy.reader(csv_file_2) as csv:
        assert csv.has_header is True
        assert csv.dialect.delimiter == ','

        gen = csv.iter(skip_header=False, namedtuple=True)

        i, r = next(gen)
        assert i == 0
        assert r.a == "A"
        assert r.column_1 == ""
        assert r.c == "C"
        assert r.column_3rd == "3rd"
        assert r.a_4 == "A"

        i, r = next(gen)
        assert i == 1
        assert r.a == "one"
        assert r.column_1 == "two"
        assert r.c == "three"
        assert r.column_3rd == "four"
        assert r.a_4 == "four.one"

        i, r = next(gen)
        assert i == 2
        assert r.a == "five"
        assert r.column_1 == "six"
        assert r.c == "seven"
        assert r.column_3rd == "eight"
        assert r.a_4 == "eight.one"

        with pytest.raises(StopIteration):
            next(gen)

def test_csvy_read_with_namedtuple(csv_file):
    CsvRow = _csvy.build_named_tuple(("A", "B", "C"))
    with csvy.reader(csv_file) as csv:
        assert csv.has_header is True
        assert csv.dialect.delimiter == ','

        gen = csv.iter()

        i, r = next(gen)
        assert i == 1
        assert r == CsvRow(a="one", b="two", c="three")
